//
//  Review.m
//  InApp
//
//  Created by admin on 2020/6/1.
//  Copyright © 2020 admin. All rights reserved.
//

#import "Review.h"
#import <StoreKit/StoreKit.h>

@implementation Review

///public method
+ (void)review:(NSString *)appleID {
    if (@available(iOS 10.3, *)) {
        [self reviewInApp];
    } else {
        [self reviewInAppstore:appleID];
    }
}

///privite method
+ (void)reviewInApp {
    if (@available(iOS 10.3, *)) {
        [SKStoreReviewController requestReview];
    }
}

///privite method
+ (void)reviewInAppstore:(NSString *)appleID {
    NSDictionary *infoDictionary = NSBundle.mainBundle.infoDictionary;
    NSString *appName = [infoDictionary objectForKey:@"CFBundleDisplayName"] ?: [infoDictionary objectForKey:@"CFBundleName"];
    NSString *message = [NSString stringWithFormat:@"為了更好地提升我們的產品與服務，希望您可以對%@的使用進行評價。您的寶貴回饋，將說明我們提供更優質的客戶體驗！只需要一分鐘的時間。", appName];
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"撰寫評價" message:message preferredStyle:UIAlertControllerStyleAlert];

    UIAlertAction *evaluationAction = [UIAlertAction actionWithTitle:@"立即評價"
                                                               style:UIAlertActionStyleDefault
                                                             handler:^(UIAlertAction *_Nonnull action) {
                                                                 if ([self shouldTerminalReview]) {
                                                                     return;
                                                                 }
                                                                 NSString *reviewLink = [NSString stringWithFormat:@"itms-apps://itunes.apple.com/app/id%@?action=write-review", appleID];
                                                                 NSURL *appReviewUrl = [NSURL URLWithString:reviewLink];
                                                                 if (@available(iOS 10.0, *)) {
                                                                     [[UIApplication sharedApplication] openURL:appReviewUrl options:@{} completionHandler:nil];
                                                                 } else {
                                                                     [[UIApplication sharedApplication] openURL:appReviewUrl];
                                                                 }
                                                             }];
    UIAlertAction *remindAction = [UIAlertAction actionWithTitle:@"稍後提醒"
                                                           style:UIAlertActionStyleDefault
                                                         handler:^(UIAlertAction *_Nonnull action){

                                                         }];
    UIAlertAction *goodbyeAction = [UIAlertAction actionWithTitle:@"不，謝謝"
                                                            style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction *_Nonnull action){

                                                          }];
    [alert addAction:evaluationAction];
    [alert addAction:remindAction];
    [alert addAction:goodbyeAction];
    UIViewController *root = nil;
    if (@available(iOS 13.0, *)) {
        root = UIApplication.sharedApplication.windows.firstObject.rootViewController;
    } else {
        root = UIApplication.sharedApplication.keyWindow.rootViewController;
    }
    [root presentViewController:alert animated:YES completion:nil];
}

///privite method
+ (BOOL)isSimuLator {
    if (TARGET_IPHONE_SIMULATOR == 1 && TARGET_OS_IPHONE == 1) {
        //模拟器
        return YES;
    } else {
        //真机
        return NO;
    }
}

///privite method
+ (BOOL)shouldTerminalReview {
    if ([self isSimuLator]) {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"溫馨提示" message:@"模擬器不支持跳轉AppStore" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *sureAction = [UIAlertAction actionWithTitle:@"確認"
                                                             style:UIAlertActionStyleDefault
                                                           handler:^(UIAlertAction *_Nonnull action){

                                                           }];
        [alert addAction:sureAction];
        UIViewController *root = nil;
        if (@available(iOS 13.0, *)) {
            root = UIApplication.sharedApplication.windows.firstObject.rootViewController;
        } else {
            root = UIApplication.sharedApplication.keyWindow.rootViewController;
        }
        [root presentViewController:alert animated:YES completion:nil];
        return YES;
    } else {
        return NO;
    }
}

@end
