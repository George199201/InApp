//
//  Review.h
//  InApp
//
//  Created by admin on 2020/6/1.
//  Copyright © 2020 admin. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface Review : NSObject

///10.3以上版本弹出应用内评分
///10.3以下版本弹出应用商店评分，此时需要用到苹果商店的应用ID
+ (void)review:(NSString *)appleID API_AVAILABLE(ios(8.0));

@end

NS_ASSUME_NONNULL_END
