//
//  Share.m
//  InApp
//
//  Created by admin on 2020/6/1.
//  Copyright © 2020 admin. All rights reserved.
//

#import "Share.h"
#import <UIKit/UIKit.h>

@implementation Share

///应用内分享(邀请)
+(void)share:(NSString *)title link:(NSString *)link {
    //分享的标题
    NSString *shareTitle = title ?: @"分享标题不能为空";
    //分享的图片
    
    //分享的链接
    NSString *shareLink = link ?: @"https://www.baidu.com";
    NSURL *shareURL = nil;
    if (shareLink && shareLink.length > 0) {
        shareURL = [NSURL URLWithString:shareLink];
    }
    do {
        if (!shareTitle) break;
        //        if (!sharePicture) break;
        if (!shareURL) break;

        NSArray *activityItems = @[ shareTitle, shareURL ];
        UIActivityViewController *activityController = [[UIActivityViewController alloc] initWithActivityItems:activityItems applicationActivities:nil];
        //可以隐藏不需要显示的项目
        //activityController.excludedActivityTypes = @[UIActivityTypePrint, UIActivityTypeCopyToPasteboard, UIActivityTypeAssignToContact, UIActivityTypeSaveToCameraRoll];
        UIViewController *root = UIApplication.sharedApplication.keyWindow.rootViewController;
        [root presentViewController:activityController animated:YES completion:nil];
        activityController.completionWithItemsHandler = ^(UIActivityType activityType, BOOL completed, NSArray *returnedItems, NSError *activityError) {
            if (completed) {
                NSLog(@"分享成功");
            } else {
                NSLog(@"分享取消");
            }
        };
    } while (0);
}

@end
