//
//  InApp.h
//  InApp
//
//  Created by admin on 2020/6/1.
//  Copyright © 2020 admin. All rights reserved.
//

#import <Foundation/Foundation.h>

//! Project version number for InApp.
FOUNDATION_EXPORT double InAppVersionNumber;

//! Project version string for InApp.
FOUNDATION_EXPORT const unsigned char InAppVersionString[];

NS_ASSUME_NONNULL_BEGIN

@interface InApp : NSObject

///10.3以上版本弹出应用内评分
///10.3以下版本弹出应用商店评分，此时需要用到苹果商店的应用ID
+ (void)review:(NSString *)appleID API_AVAILABLE(ios(8.0));

///应用内分享(邀请)
+(void)share:(NSString *)title link:(NSString *)link API_AVAILABLE(ios(8.0));

@end

NS_ASSUME_NONNULL_END
