//
//  Share.h
//  InApp
//
//  Created by admin on 2020/6/1.
//  Copyright © 2020 admin. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface Share : NSObject

///应用内分享(邀请)
+(void)share:(NSString *)title link:(NSString *)link;

@end

NS_ASSUME_NONNULL_END
