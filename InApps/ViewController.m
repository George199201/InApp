//
//  ViewController.m
//  InApp
//
//  Created by admin on 2020/6/1.
//  Copyright © 2020 admin. All rights reserved.
//

#import "ViewController.h"
#import <InApp/InApp.h>

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//        [InApp review:@""];
        [InApp share:@"测试" link:@"https://www.baidu.com"];
    });
}


@end
